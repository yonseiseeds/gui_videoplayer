import os
import copy as cp
from PyQt5.QtCore import Qt, QDir, QPoint, QUrl
from PyQt5.QtWidgets import QFileDialog, QInputDialog, QMainWindow
from PyQt5.QtMultimedia import QMediaContent
from moviepy.editor import VideoFileClip
from player_reorganize.clip_editor import ClipEditor


class ClipBoard:

    def __init__(self):
        """
        type      : Variable indicating data type of the saved
        object    : Pointer of the saved
        clip_name : Clip's name of the saved

        seq_bar_key : Key value of sequence label  ex) "Video 1", "Audio 2"
        seq_bar_idx : Index of sequence label
        x_pos       : Start position of sequence label in pixel
        duration    : Duration of sequence label

        start_time : Start position of seleceted frame bar in time
        end time   : End position of selected frame bar in time
        """

        self.type = None
        self.object = None
        self.clip_name = None
        self.seq_bar_key = None
        self.seq_bar_idx = None
        self.x_pos = None
        self.duration = None
        self.start_time = None
        self.end_time = None


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        # List of file extensions
        self.extDic = ['mp4', 'avi', 'ogv', 'png', 'webm', 'wmv']

        # Clip Board variables
        self.lastClick = ClipBoard()  # Changed in every click
        self.copyBoard = ClipBoard()  # Changed when cut or copy activated

        self.clip_editor = ClipEditor()
        self.setCentralWidget(self.clip_editor)

        self.statusBar()
        self.make_menu_bar()
        self.home_path = QDir.homePath()

        # Connect Methods and Buttons
        self.connect_function()

        self.setWindowTitle('Video Editor')
        self.setGeometry(150, 50, 1440, 800)
        self.show()

    def open_file(self):

        """
        Open Video File and Add to Clip list

        :return:
        """

        player = self.clip_editor.video_player
        clip_list = player.list.list
        ctrl_btn = player.clipCtrl.ctrlBtn
        io_btn = player.list.ioButton

        # Get file name from dialog
        file_names, _ = QFileDialog.getOpenFileNames(caption="Open Files")

        for name in file_names:
            if name.split(".")[-1] in self.extDic:

                # Initial name is the name of video file
                clip_name = os.path.split(name)[-1]
                temp_url = QUrl(clip_name)

                # Check is there clip with same name
                name_check = False
                while not name_check:
                    name_check = True

                    for idx_list in range(clip_list.mediaCount()):
                        if clip_list.media(idx_list) == QMediaContent(temp_url):
                            name_check = False  # There is same name
                            # Get new name
                            clip_name, input_check = QInputDialog.\
                                getText(self, "Input Clip Name", "clip: ")

                            # If dialog canceled, Do Nothing
                            if not input_check:
                                return

                            # Renew Url
                            temp_url = QUrl(clip_name)

                # Make video clip and Add to list
                player.clip[clip_name] = VideoFileClip(name)
                clip_list.addMedia(QMediaContent(temp_url))

                # Activate or Deactivate buttons
                if clip_list.isEmpty():
                    ctrl_btn.activate_button(False)
                    io_btn.activate_button(False)
                else:
                    ctrl_btn.activate_button(True)
                    io_btn.activate_button(True)

    def remove_from_list(self):

        """
        Remove from list and Destruct selected clip object

        :return:
        """

        player = self.clip_editor.video_player
        clip_list = player.list.list

        # If there isn't selected clip, Do Nothing
        if clip_list.isEmpty() or clip_list.currentIndex() == -1:
            self.setStatusTip("Not Selected Clip")
            return
        else:
            idx = clip_list.currentIndex()
            clip_name = clip_list.currentMedia().canonicalUrl().fileName()

        # Stop Playing
        player.stop_clip()
        player.signal.quitPlay = True

        # Remove from list and Destruct clip
        clip_list.removeMedia(idx)
        del player.clip[clip_name]

        # If list is empty, Deactivate buttons
        if clip_list.isEmpty():
            player.clipCtrl.ctrlBtn.activate_button(False)
            player.list.ioButton.activate_button(False)

    def init_temp_dir(self):

        """
        Set home directory

        :return:
        """

        self.homePath = QFileDialog. \
            getExistingDirectory(self, "Open Folder")

    def make_menu_bar(self):

        """
        Make Menu bar of the window

        :return:
        """

        from PyQt5.QtWidgets import QAction

        editor = self.clip_editor
        player = editor.video_player
        clip_list = player.list.list
        seq_widget = editor.seq_widget
        lbl_list = seq_widget.clip_label_list

        def add(target, menu, label, status, shortcut=None, fct=None):

            """
            Add new sub-menu to the menu bar

            :param target: Pointer of menuBar's parent
            :param menu: Pointer of menu
            :param label: Name of sub-menu
            :param status: Status Tip that shows on status bar
            :param shortcut: Keyboard shortcut
            :param fct: Connected function

            :return:
            """

            sub_menu = QAction(label, target)
            sub_menu.setStatusTip(status)
            sub_menu.setShortcut(shortcut)
            if fct is not None:
                sub_menu.triggered.connect(fct)
            menu.addAction(sub_menu)

        def cut():

            """
            Save lastClick object to CopyBoard object and delete the selected

            :return:
            """

            # Shallow Copy
            self.copyBoard = cp.copy(self.lastClick)

            if self.lastClick.type == "Sequence Label":
                key = self.lastClick.seq_bar_key
                idx = self.lastClick.seq_bar_idx
                seq_widget.clip_label_list[key][idx].hide()
                del seq_widget.clip_label_list[key][idx]

        def copy():

            """
            Save lastClick object to CopyBoard

            :return:
            """

            # Shallow Copy
            self.copyBoard = cp.copy(self.lastClick)

        def paste():

            """
            Paste data of CopyBoard object to selected area

            :return:
            """

            if self.copyBoard.type == "Sequence Label":
                clip_name = self.copyBoard.clip_name
                duration = self.copyBoard.duration
                draw_v = (self.copyBoard.seq_bar_key[:-2] == "Video")

                # Do NOT draw video and audio at same time
                draw_a = not draw_v

                # Draw label in sequence bar
                seq_widget.make_label(clip_name, duration, draw_video=draw_v,
                                      draw_audio=draw_a)

        def make_label():

            """
            Draw Label to the Sequence Bar

            :return:
            """

            # If there isn't selected clip, Do Nothing
            if clip_list.isEmpty() or clip_list.currentIndex() == -1:
                self.setStatusTip("Not Selected Clip")
                return
            else:
                # Get clip name
                clip_name = clip_list.currentMedia().canonicalUrl().fileName()

            # Draw label
            seq_widget.make_label(clip_name, player.clip[clip_name].duration)

        def divide_label():

            """
            Divide Label using Sequence Cursor(Red Line) in Sequence Bar

            :return:
            """

            seq_idx = seq_widget.line.combobox.currentIndex() + 1

            for key in ["Video " + str(seq_idx), "Audio " + str(seq_idx)]:
                for i in range(len(lbl_list[key])):

                    # Get position of left Side and right side of label
                    left = lbl_list[key][i].x() / 10
                    right = left + lbl_list[key][i].width() / 10

                    # Get position of sequence cursor
                    seq_time = seq_widget.line.sequence_cursor_time

                    if left <= seq_time <= right:

                        # Get time info
                        bgn = 0
                        mid = seq_time - left
                        end = right - left

                        clip_name = lbl_list[key][i].name

                        editor.trim(clip_name, bgn, mid)
                        editor.trim(clip_name, mid, end)

                        # Save data for 'Cut()'
                        self.lastClick.type = "Sequence Label"
                        self.lastClick.seq_bar_key = key
                        self.lastClick.seq_bar_idx = i

                        cut()

                        # Calculate name of trimmed clips
                        name1 = str('(%.2f : %.2f)' % (bgn, mid)) + clip_name
                        name2 = str('(%.2f : %.2f)' % (mid, end)) + clip_name

                        # Draw label
                        seq_widget.make_label(name1, mid - bgn, left)
                        seq_widget.make_label(name2, end - mid, seq_time)

        def trim():

            """
            Trim Video from Frame Bar or Clip

            :return:
            """

            # Initially save meaningless data
            start, end = -1, -1

            # If there isn't selected clip, Do Nothing
            if clip_list.isEmpty() or clip_list.currentIndex() == -1:
                self.setStatusTip("Not Selected Clip")
                return
            else:
                clip_name = clip_list.currentMedia().canonicalUrl().fileName()

            # If frame bar is lastly clicked, Get time info from frame bar
            if self.lastClick.type == "Frame Bar":
                if self.lastClick.start_time is not None:
                    start = self.lastClick.start_time
                if self.lastClick.end_time is not None:
                    end = self.lastClick.end_time

            # If time info is Not updated, Get from dialog
            if start == -1:
                start, _ = QInputDialog.getDouble(self, "Get Start Time",
                                                  "Input Start time")
            if end == -1:
                end, _ = QInputDialog.getDouble(self, "Get End Time",
                                                "Input End Time")

            # Trim video
            editor.trim(clip_name, start_time=start, end_time=end)

        def set_frame_bar():

            """
            Draw Frame Bar

            :return:
            """

            # If there isn't selected clip, Do Nothing
            if clip_list.isEmpty() or clip_list.currentIndex() == -1:
                self.setStatusTip("Not Selected Clip")
                return
            else:
                clip_name = clip_list.currentMedia().canonicalUrl().fileName()

            # Remake frame bar
            player.frameBar.set_frame(player.clip[clip_name])

        menu_bar = self.menuBar()

        # File Menu
        file_menu = menu_bar.addMenu('&File')
        add(self, file_menu, 'Open', 'Open video file', 'Shift+Ctrl+O',
            fct=self.open_file)
        add(self, file_menu, 'Set Path', 'Set directory', 'Ctrl+P',
            fct=self.init_temp_dir)
        file_menu.addSeparator()
        add(self, file_menu, 'ScreenShot', 'Take ScreenShot', 'F12',
            fct=editor.screenshot)

        # Edit Menu
        edit_menu = menu_bar.addMenu('&Edit')
        add(self, edit_menu, 'Remove', 'Remove from List', 'Shift+Ctrl+T',
            fct=self.remove_from_list)
        edit_menu.addSeparator()
        add(self, edit_menu, ' Cut', 'Cut Selected Video', 'Ctrl+X',
            fct=cut)
        add(self, edit_menu, ' Copy', 'Copy Selected Video', 'Ctrl+C',
            fct=copy)
        add(self, edit_menu, ' Paste', 'Paste Selected Video', 'Ctrl+V',
            fct=paste)
        edit_menu.addSeparator()
        add(self, edit_menu, 'Make Label',
            'Make Label in Sequence Bar of Current Clip', 'Ctrl+N',
            fct=make_label)
        add(self, edit_menu, 'Make Sequence',
            'Make New Sequence in Sequence Bar', 'Shift+Ctrl+N',
            fct=seq_widget.make_sequence)
        add(self, edit_menu, 'Divide Label', 'Divide Label in two', 'Ctrl+D',
            fct=divide_label)
        add(self, edit_menu, 'Trim', 'Trim Video', 'Ctrl+T',
            fct=trim)

        # View Menu
        view_menu = menu_bar.addMenu('&View')
        add(self, view_menu, 'Resize FrameBar', 'Remake FrameBar', 'Ctrl+R',
            fct=set_frame_bar)

    def connect_function(self):

        """
        Connect Buttons and Functions

        :return:
        """

        player = self.clip_editor.video_player
        frm_bar = player.frameBar
        ctrl_btn = player.clipCtrl.ctrlBtn
        io_btn = player.list.ioButton
        slider = player.clipCtrl.posBar.posSlider

        def prev_btn():
            player.move_position_clip(position=-5, rel_pos=True)

        def next_btn():
            player.move_position_clip(position=+5, rel_pos=True)

        def pos_bar_moved(pos=None):
            if pos is None:
                pos = slider.sliderPosition()
            player.move_position_clip(position=pos, rel_pos=False)

        # Connect file IO button
        io_btn.addButton.clicked.connect(self.open_file)
        io_btn.removeButton.clicked.connect(self.remove_from_list)

        # Connect clip control button
        ctrl_btn.playButton.clicked.connect(player.play_clip)
        ctrl_btn.stopButton.clicked.connect(player.stop_clip)
        ctrl_btn.prevButton.clicked.connect(prev_btn)
        ctrl_btn.nextButton.clicked.connect(next_btn)
        ctrl_btn.frameResetButton.clicked.connect(frm_bar.selected.reset)

        # Connect position slider
        slider.sliderMoved.connect(pos_bar_moved)
        slider.sliderReleased.connect(pos_bar_moved)

    # Overload Qt Methods
    def mousePressEvent(self, QMouseEvent):

        """
        Overload Qt Method
        Called if mouse button clicked

        :param QMouseEvent: Original input of overloded function
        :return:
        """

        def is_in(obj, x_pos=None, y_pos=None):

            """
            If positions are in the object area, return True.
            If out of the area, return False

            :param obj: object
            :param x_pos: Position of x. If None, x-pos = x-mouse
            :param y_pos: Position of y. If None, y-pos = y-mouse

            :return: True or False
            """

            if x_pos is None:
                x_pos = x_mouse
            if y_pos is None:
                y_pos = y_mouse

            return obj.x() <= x_pos <= obj.x() + obj.width() \
                and obj.y() <= y_pos <= obj.y() + obj.height()

        def x_is_near(obj, near=5, x_pos=None):

            """
            "Magnet" Function
            If x-pos is near obj's right or left edge, return that edge position
            If not, return x-pos

            :param obj: object
            :param near: If there is less than 'near' pixel difference,
                        it is judged to be close
            :param x_pos: Position of x. If None, x-pos = x-mouse

            :return: obj's right or left corner coordinate Or x-pos coordinate
            """

            # Calculate left and right corner coordinate
            left = obj.x()
            right = obj.x() + obj.width()

            if x_pos is None:
                x_pos = x_mouse

            if left - near <= x_pos <= left + near:
                return left
            elif right - near <= x_pos <= right + near:
                return right
            else:
                return x_pos

        def remember_clicked(type=None, obj=None, clip_name=None,
                             key=None, idx=None, xpos=None, duration=None,
                             start=None, end=None):

            """
            Save clicked information to Clip Board variable 'lastClick'

            :return:
            """

            # General info
            self.lastClick.type = type
            self.lastClick.object = obj
            self.lastClick.clip_name = clip_name

            # Sequence bar info
            self.lastClick.seq_bar_key = key
            self.lastClick.seq_bar_idx = idx
            self.lastClick.x_pos = xpos
            self.lastClick.duration = duration

            # Frame bar info
            self.lastClick.start_time = start
            self.lastClick.end_time = end

        # Initialize lastClick variable
        remember_clicked()

        if QMouseEvent.buttons() == Qt.LeftButton:

            x_mouse = QPoint.x(QMouseEvent.pos())
            y_mouse = QPoint.y(QMouseEvent.pos())

            player = self.clip_editor.video_player
            clip_list = player.list.list
            current_clip = clip_list.currentMedia().canonicalUrl().fileName()
            frame = player.frameBar
            pos_bar = player.clipCtrl.posBar
            duration = pos_bar.duration
            seq_widget = self.clip_editor.seq_widget
            slider_pos = seq_widget.line.scroll_bar.sliderPosition()
            inner_w = seq_widget.time.line_inner.width()
            widget_w = seq_widget.time.line.width()

            # For Clip Play Widget
            if is_in(player.clipWidget):
                player.play_clip()
                return

            # For Frame Bar
            if is_in(frame.bar):
                remember_clicked(type="Frame Bar",
                                 obj=frame.bar, clip_name=current_clip,
                                 start=frame.selected.point1,
                                 end=frame.selected.point2)

                # Relative x coordinate in frame bar
                x_pix_pos = x_mouse - frame.bar.x()

                # Time-wise coordinate
                x_time_pos = x_pix_pos * duration / frame.bar.width()

                # Move position slider
                player.move_position_clip(x_time_pos)

                # If set button is pressed, select area
                if player.clipCtrl.ctrlBtn.frameSetButton.isChecked():
                    frame.selected.select_point(x_time_pos, duration)

                return

            # For Sequence Bar
            if is_in(seq_widget.time.line):

                # Relative x coordinate in sequence bar
                x_pix_pos = slider_pos * (inner_w - widget_w) / 999 \
                    + x_mouse - seq_widget.time.line.x()
                # Time-wise coordinate (10 pixel is 1 sec)
                x_time_pos = x_pix_pos / 10

                # Magnet function
                for key in seq_widget.line.keys:
                    lbl_list = seq_widget.clip_label_list[key]
                    n_labels = len(lbl_list)
                    for i in range(n_labels):
                        x_pix_pos = x_is_near(lbl_list[i], 5, x_pix_pos)

                # Move cursor and update its time-wise position
                seq_widget.line.move_seq_cursor(x_pix_pos)
                seq_widget.line.sequence_cursor_time = x_time_pos

                return

            # For Sequence Labels
            for key in seq_widget.line.keys:

                # Left side of sequence bar
                if is_in(seq_widget.line.label[key]):

                    # Set combobox index to clicked sequence
                    seq_widget.line.combobox.setCurrentIndex(int(key[-1]) - 1)

                # Right side of sequence bar
                if is_in(seq_widget.line.sequence[key]):

                    # Set combobox index to clicked sequence
                    seq_widget.line.combobox.setCurrentIndex(int(key[-1]) - 1)

                    # Relative x and y coordinate in sequence bar
                    x = slider_pos * (inner_w - widget_w) / 999 \
                        + x_mouse - seq_widget.line.sequence[key].x()
                    y = y_mouse - seq_widget.line.sequence[key].y()

                    # If a label is clicked, save its info
                    lbl_list = seq_widget.clip_label_list[key]
                    n_labels = len(lbl_list)
                    for i in range(n_labels):
                        if is_in(lbl_list[i], x, y):
                            remember_clicked(
                                type="Sequence Label",
                                obj=lbl_list[i], clip_name=lbl_list[i].name,
                                key=key, idx=i,
                                xpos=lbl_list[i].x(),
                                duration=lbl_list[i].width() / 10
                            )

                            return
                        return

    def mouseMoveEvent(self, QMouseEvent):
        self.mousePressEvent(QMouseEvent)


if __name__ == "__main__":
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
