from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QWidget
from PyQt5.QtMultimedia import QMediaContent
from player_reorganize.clip_player import ClipPlayer
from player_reorganize.sequence_bar import SequenceWidget


class ClipEditor(QWidget):

    def __init__(self):
        super().__init__()

        self.video_player = ClipPlayer()

        self.seq_widget = SequenceWidget()

        self.make_layout()

    def screenshot(self, clip_name=None, time=None):

        """
        Save image of argumented clip and time

        :param clip_name: Name of clip. If None, current clip will be used
        :param time: Time position. If None, current time will be used
        :return:
        """

        player = self.video_player
        clip_list = player.list.list

        # Pause
        if player.signal.keepPlay is True:
            player.play_clip()

        # Get name
        if clip_name is None:

            # If There isn't Selected Clip, Do Nothing
            if clip_list.isEmpty() or clip_list.currentIndex() == -1:
                self.setStatusTip("Not Selected Clip")
                return
            else:
                clip_name = clip_list.currentMedia().canonicalUrl().fileName()

        # Get time
        if time is None:
            time = player.clipCtrl.posBar.posSlider.value()

        img = player.clip[clip_name].to_ImageClip(time)
        name = './screenshot_' + clip_name + '_' + str(time) + '.jpg'

        # Save Image File
        img.save_frame(name)

    def trim(self, clip_name, start_time=-1, end_time=-1, new_name=None):

        """
        Trim clip[clip-name] from start-time to end-time

        :param clip_name: Name of clip. If None, current clip will be used
        :param start_time: Start time of trimmed clip
        :param end_time: End time of trimmed clip
        :param new_name: New name of trimmed clip
        :return:
        """

        player = self.video_player
        clip_list = player.list.list

        # Exception handling
        if clip_list.isEmpty() and clip_list.currentIndex() == -1:
            return

        if clip_name is None:
            clip_name = clip_list.currentMedia().canonicalUrl().fileName()
        clip = player.clip[clip_name]
        duration = clip.duration

        # Bound times
        if start_time < 0 or start_time > duration:
            start_time = 0
        if end_time < 0 or end_time > duration:
            end_time = duration

        # Make new name for trimmed clip
        if new_name is None:
            new_name = str('(%.2f : %.2f)' % (start_time, end_time)) \
                       + clip_name

        # Check is there clip with same name
        for i in range(clip_list.mediaCount()):
            if clip_list.media(i) == QMediaContent(QUrl(new_name)):
                return

        # Trim clip and Add to clip list
        try:
            player.clip[new_name] = clip.subclip(start_time, end_time)
            clip_list.addMedia(QMediaContent(QUrl(new_name)))
        except AttributeError:
            pass

    def make_layout(self):
        from PyQt5.QtWidgets import QVBoxLayout

        self.layout = QVBoxLayout()
        self.layout.addLayout(self.video_player.layout)
        self.layout.addLayout(self.seq_widget.layout)

        self.setLayout(self.layout)
