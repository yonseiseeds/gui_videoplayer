from PyQt5.QtCore import Qt, QTime
from PyQt5.QtWidgets import QWidget, QSlider, QStyle


class ClipControlButton(QWidget):

    def __init__(self):
        from PyQt5.QtWidgets import QPushButton

        super().__init__()

        self.playButton = QPushButton()
        self.stopButton = QPushButton()
        self.nextButton = QPushButton()
        self.prevButton = QPushButton()

        self.volumeSlider = QSlider(Qt.Horizontal)
        self.volumeSlider.setRange(0, 100)
        self.volumeSlider.setValue(100)

        self.frameSetButton = QPushButton("Set")
        self.frameResetButton = QPushButton("Reset")

        # Setup Buttons
        self.setup_button()
        self.activate_button(False)

        self.make_layout()

    def setup_button(self):

        s_icon = self.style().standardIcon

        self.playButton.setIcon(s_icon(QStyle.SP_MediaPlay))
        self.stopButton.setIcon(s_icon(QStyle.SP_MediaStop))
        self.nextButton.setIcon(s_icon(QStyle.SP_MediaSeekForward))
        self.prevButton.setIcon(s_icon(QStyle.SP_MediaSeekBackward))

        self.playButton.setShortcut("Space")

        self.frameSetButton.setCheckable(True)

    def set_pause_icon(self):
        s_icon = self.style().standardIcon
        self.playButton.setIcon(s_icon(QStyle.SP_MediaPause))

    def set_play_icon(self):
        s_icon = self.style().standardIcon
        self.playButton.setIcon(s_icon(QStyle.SP_MediaPlay))

    def activate_button(self, activate=True):
        self.playButton.setEnabled(activate)
        self.stopButton.setEnabled(activate)
        self.nextButton.setEnabled(activate)
        self.prevButton.setEnabled(activate)

    def make_layout(self):
        from PyQt5.QtWidgets import QHBoxLayout

        self.layout = QHBoxLayout()
        self.layout.addWidget(self.prevButton)
        self.layout.addWidget(self.stopButton)
        self.layout.addWidget(self.playButton)
        self.layout.addWidget(self.nextButton)
        self.layout.addWidget(self.volumeSlider)


class PositionBar(QWidget):

    def __init__(self):
        from PyQt5.QtWidgets import QLabel

        super().__init__()

        self.duration = 0

        self.labelDuration = QLabel('00:00/00:00')
        self.labelDuration.setAttribute(Qt.WA_TranslucentBackground, True)

        self.posSlider = QSlider(Qt.Horizontal)
        self.posSlider.setRange(0, self.duration)

    def update_duration_info(self, current_info):

        """
        Update time label

        :param current_info: current time position of the clip
        :return:
        """

        duration = self.duration

        time_str = ""

        # Make time string from time info
        if current_info or duration:
            current = QTime((current_info / 3600) % 60,
                            (current_info / 60) % 60,
                            current_info % 60, current_info)
            total = QTime((duration / 3600) % 60,
                          (duration / 60) % 60,
                          duration % 60, duration)

            t_form = 'hh:mm:ss' if duration > 3600 else 'mm:ss'
            time_str = current.toString(t_form) + "/" + total.toString(t_form)

        self.labelDuration.setText(time_str)


class ClipControl(QWidget):

    def __init__(self):

        super().__init__()

        self.ctrlBtn = ClipControlButton()
        self.posBar = PositionBar()

        self.make_layout()

    def make_layout(self):
        from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout

        # Set Control Buttons and Time Label in a row
        ctrl_layout = QHBoxLayout()
        ctrl_layout.addLayout(self.ctrlBtn.layout)
        ctrl_layout.addWidget(self.posBar.labelDuration)
        ctrl_layout.addStretch(1)
        ctrl_layout.addWidget(self.ctrlBtn.frameSetButton)
        ctrl_layout.addWidget(self.ctrlBtn.frameResetButton)

        # Set Position Slider above Control Buttons
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.posBar.posSlider)
        self.layout.addLayout(ctrl_layout)
