from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QImage, QPainter, QPixmap
from PyQt5.QtWidgets import QWidget, QFrame, QLabel, QGraphicsOpacityEffect
from qwt.toqimage import array_to_qimage


class Selected(QWidget):

    def __init__(self, target):

        """
        pointA: Point that selected first
        pointB: Point that selected latter

        point1: Former point in time between pointA and B
        point2: Latter point in time between pointA and B

        line1,2: Green line on point1,2
        area: Blue area between two green lines

        :param target: Variable to get width of parent
        """

        def make_line():

            """
            Make two green line on frame bar

            :return: line widget
            """

            line = QFrame(target)
            line.setStyleSheet("background-color: green")
            line.setFrameShadow(QFrame.Plain)
            line.setFrameShape(QFrame.VLine)
            line.setLineWidth(-1)
            return line

        def make_area():

            """
            Make blue area between two green lines

            :return: blue area
            """

            area = QLabel(target)
            opacity = QGraphicsOpacityEffect(area)
            opacity.setOpacity(0.3)
            area.setStyleSheet("background-color: blue")
            area.setGraphicsEffect(opacity)
            return area

        super().__init__()

        self.target = target

        self.pointA = 0
        self.pointB = 0
        self.point1 = self.pointA
        self.point2 = self.pointB
        self.line1 = make_line()
        self.line2 = make_line()
        self.area = make_area()

        self.select_point()

    def select_point(self, pos=0, duration=1):

        """
        Change position of pointA or B.

        :param pos: New position
        :param duration: Duration of clip or width of label
        :return:
        """

        # Coefficient connect time and pixel position
        coeff = self.target.width() / duration

        def is_near(point, near=5):

            """
            Check if pos is near by point

            :param point: PointA or B
            :param near: Criteria in pixel dimension

            :return: bool, True if pos and point is close to each other
            """

            return point - near <= pos <= point + near

        def move_line(line, point):

            """
            Move green line to new position

            :param line: Pointer of line1 or 2
            :param point: new position
            :return:
            """

            line.setGeometry(point * coeff, 0, 5, 50)

        def draw_area(area, p1, p2):

            """
            Draw blue area between two green line

            :param area: Pointer of area
            :param p1: Position of line1
            :param p2: Position of line2
            :return:
            """

            area.setGeometry(p1 * coeff, 0, (p2 - p1) * coeff, 50)

        # Change pointA or B
        # If pos has difference within 5 pix to pointA or pointB,
        if is_near(self.pointA):
            self.pointA = pos
        elif is_near(self.pointB):
            self.pointB = pos

        # If not, pointB will be changed
        else:
            self.pointA = self.pointB
            self.pointB = pos

        # Align in Temporal Order
        if self.pointA < self.pointB:
            self.point1 = self.pointA
            self.point2 = self.pointB
        else:
            self.point1 = self.pointB
            self.point2 = self.pointA

        # Draw lines and area
        move_line(self.line1, self.point1)
        move_line(self.line2, self.point2)
        draw_area(self.area, self.point1, self.point2)

    def reset(self):

        """
        Set two position to start point

        :return:
        """

        self.pointA = 0
        self.pointB = 0

        self.select_point()


class FrameBar(QWidget):

    def __init__(self):

        super().__init__()

        # Initiate Label for Show Frames of Clip
        self.bar = QLabel()
        self.bar.setStyleSheet("background-color: black")
        self.bar.setMinimumHeight(50)
        self.bar.setAlignment(Qt.AlignCenter)

        self.selected = Selected(self.bar)

    def set_frame(self, clip):

        """
        Draw frames in frame bar

        :param clip: current clip
        :return:
        """

        width = 80
        height = 50
        n_img = int(self.bar.width() / width) + 1

        duration = clip.duration
        t = duration / n_img      # Time Interval
        color_format = array_to_qimage(clip.get_frame(0)).format()

        for i in range(2):
            self.selected.select_point(0, duration)

        # Result Image for Frame Bar
        result_img = QImage(width * n_img, height, color_format)

        # Paint Device to Draw and Concatenate Frames
        painter = QPainter(result_img)

        size = QSize(width, height)  # Size of Each Frame

        # Get image and Concatenate them
        for i in range(n_img):
            img = array_to_qimage(clip.get_frame(i * t)).scaled(size)
            painter.drawImage(width * i, 0, img)

        painter.end()

        size = self.bar.size()  # Size of Frame Bar
        self.bar.setPixmap(QPixmap.fromImage(result_img).scaled(size))
