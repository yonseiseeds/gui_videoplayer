from PyQt5.QtCore import Qt, QAbstractItemModel, QFileInfo, QModelIndex
from PyQt5.QtWidgets import QWidget
from PyQt5.QtMultimedia import QMediaPlaylist


class PlaylistModel(QAbstractItemModel):
    Title, ColumnCount = range(2)

    def __init__(self, parent=None):
        super(PlaylistModel, self).__init__(parent)

        self.m_playlist = None

    def rowCount(self, parent=QModelIndex()):
        return self.m_playlist.mediaCount() \
            if self.m_playlist is not None and not parent.isValid() else 0

    def columnCount(self, parent=QModelIndex()):
        return self.ColumnCount if not parent.isValid() else 0

    def index(self, row, column, parent=QModelIndex()):
        return self.createIndex(row, column) \
            if self.m_playlist is not None and not parent.isValid()\
               and row >= 0 and row < self.m_playlist.mediaCount() \
               and column >= 0 and column < self.ColumnCount else QModelIndex()

    def parent(self, child):
        return QModelIndex()

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid() and role == Qt.DisplayRole:
            if index.column() == self.Title:
                location = self.m_playlist.media(index.row()).canonicalUrl()
                return QFileInfo(location.path()).fileName()
            return self.m_data[index]
        return None

    def playlist(self):
        return self.m_playlist

    def set_playlist(self, playlist):
        if self.m_playlist is not None:
            self.m_playlist.mediaAboutToBeInserted.disconnect(
                self.beginInsertItems)
            self.m_playlist.mediaInserted.disconnect(self.endInsertItems)
            self.m_playlist.mediaAboutToBeRemoved.disconnect(
                self.beginRemoveItems)
            self.m_playlist.mediaRemoved.disconnect(self.endRemoveItems)
            self.m_playlist.mediaChanged.disconnect(self.changeItems)

        self.beginResetModel()
        self.m_playlist = playlist

        if self.m_playlist is not None:
            self.m_playlist.mediaAboutToBeInserted.connect(
                self.beginInsertItems)
            self.m_playlist.mediaInserted.connect(self.endInsertItems)
            self.m_playlist.mediaAboutToBeRemoved.connect(
                self.beginRemoveItems)
            self.m_playlist.mediaRemoved.connect(self.endRemoveItems)
            self.m_playlist.mediaChanged.connect(self.changeItems)

        self.endResetModel()

    def beginInsertItems(self, start, end):
        self.beginInsertRows(QModelIndex(), start, end)

    def endInsertItems(self):
        self.endInsertRows()

    def beginRemoveItems(self, start, end):
        self.beginRemoveRows(QModelIndex(), start, end)

    def endRemoveItems(self):
        self.endRemoveRows()

    def changeItems(self, start, end):
        self.dataChanged.emit(self.index(start, 0),
                              self.index(end, self.ColumnCount))


class IOButton(QWidget):

    def __init__(self):
        from PyQt5.QtWidgets import QPushButton

        super().__init__()

        self.addButton = QPushButton("Add")
        self.removeButton = QPushButton("Remove")

        self.activate_button(False)

        self.make_layout()

    def activate_button(self, activate=True):
        self.removeButton.setEnabled(activate)

    def make_layout(self):
        from PyQt5.QtWidgets import QHBoxLayout

        self.layout = QHBoxLayout()
        self.layout.addWidget(self.addButton)
        self.layout.addWidget(self.removeButton)


class Playlist:

    def __init__(self):
        from PyQt5.QtWidgets import QListView

        self.list = QMediaPlaylist()

        self.model = PlaylistModel()
        self.model.set_playlist(self.list)

        self.listView = QListView()
        self.listView.setModel(self.model)
        self.listView.setCurrentIndex(
            self.model.index(self.list.currentIndex(), 0))
        self.listView.activated.connect(self.jump)

        self.ioButton = IOButton()

        self.make_layout()

    def jump(self, index):
        # Jump to Chosen Index
        if index.isValid():
            self.list.setCurrentIndex(index.row())

    def make_layout(self):
        from PyQt5.QtWidgets import QVBoxLayout

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.listView)
        self.layout.addLayout(self.ioButton.layout)
