import time
import threading
import pygame as pg
import numpy as np
from qwt.toqimage import array_to_qimage
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel
from player_reorganize.clip_list import Playlist
from player_reorganize.clip_control import ClipControl
from player_reorganize.clip_frame_bar import FrameBar


class ClipSignal:

    def __init__(self):

        self.position = None    # Wanted Position in Position Slider
        self.rel_pos = False    # Wanted Position is Relative or Not

        self.keepPlay = False   # Play or Not
        self.quitPlay = False   # Quit Playing Process or Not
        self.stopPlay = True    # Stop or Move to Other Time or Not

    def quit_signal(self):
        self.quitPlay = True
        self.stopPlay = False

        time.sleep(0.1)

    def play_or_pause_signal(self):
        self.keepPlay = not self.keepPlay
        self.quitPlay = False
        self.stopPlay = False

    def stop_signal(self):
        self.quitPlay = False
        self.stopPlay = True


class ClipPlayer(QWidget):

    def __init__(self):

        super().__init__()

        # Duration of clip in sec
        self.duration = 0

        self.clip = {}
        self.signal = ClipSignal()

        self.clipWidget = QLabel()
        self.clipWidget.setAlignment(Qt.AlignCenter)
        self.clipWidget.setStyleSheet("background-color: black")

        self.frameBar = FrameBar()
        self.clipCtrl = ClipControl()

        self.list = Playlist()
        self.list.list.currentIndexChanged.connect(self.list_pos_changed)

        self.play_process = threading.Thread()

        self.make_layout()

    def list_pos_changed(self, position):

        """
        Called if new clip is selected in clip list

        :param position: Index of new clip
        :return:
        """

        self.list.listView.setCurrentIndex(self.list.model.index(position, 0))

        # Pause
        self.pause_clip()

        self.signal.quit_signal()

        clip_name = self.list.list.currentMedia().canonicalUrl().fileName()
        clip = self.clip[clip_name]

        # Set Frame Bar
        self.frameBar.set_frame(clip)

        # Make New Thread and Start
        del self.play_process
        self.play_process = threading.Thread(target=self.process_play_clip)
        self.play_process.daemon = True
        self.play_process.start()

        # Play Clip
        self.play_clip()

    def pause_clip(self):

        self.signal.keepPlay = False
        self.clipCtrl.ctrlBtn.set_play_icon()

    def play_clip(self):

        self.signal.play_or_pause_signal()

        # Set to proper icon
        if self.signal.keepPlay:
            self.clipCtrl.ctrlBtn.set_pause_icon()
        else:
            self.clipCtrl.ctrlBtn.set_play_icon()

    def stop_clip(self):

        # Move to Time 0
        self.move_position_clip(position=0, rel_pos=False)
        self.clipCtrl.posBar.posSlider.setValue(0)

        # Pause
        self.signal.keepPlay = False
        self.clipCtrl.ctrlBtn.set_pause_icon()

    def move_position_clip(self, position, rel_pos=False):

        """
        Move to position or Move by position

        :param position: Relative or Absolute target position to move
        :param rel_pos: If True, position is relative. If False, absolute
        :return:
        """

        self.signal.rel_pos = rel_pos

        self.signal.stop_signal()
        self.signal.position = position

        # Since one frame is maintained 1 / 30 sec, sleep more than that
        time.sleep(0.04)

    def process_play_clip(self):

        player = self

        audiofps = 22050
        nbytes = 2

        media = player.list.list.currentMedia()
        clipname = media.canonicalUrl().fileName()
        clip = player.clip[clipname]
        fps = clip.fps
        duration = player.clipCtrl.posBar.duration = clip.duration

        # Set Maximum to Position Slider
        player.clipCtrl.posBar.posSlider.setMaximum(duration)

        def mark_time(sec):
            pos = sec
            current = time.time()
            return pos, current

        def make_image(sec):
            image = array_to_qimage(clip.get_frame(sec))
            size = self.clipWidget.size()

            # Set image to clipWidget
            player.clipWidget.setPixmap(
                QPixmap.fromImage(image).scaled(size, Qt.KeepAspectRatio))

        def make_sound(sec):

            # Time from this frame to next frame
            tt = np.arange(sec, sec + 1.0 / fps, 1.0 / audiofps)

            # Get volume from volume slider
            volume = player.clipCtrl.ctrlBtn.volumeSlider.value() / 100

            sndarray = clip.audio.to_soundarray(
                tt, nbytes=nbytes, quantize=True)
            chunk = pg.sndarray.make_sound(sndarray)

            if sec == 1.0 / fps:
                player.channel = chunk.play()
            else:
                chunk.set_volume(volume)
                while player.channel.get_queue():
                    time.sleep(0.003)
                player.channel.queue(chunk)

        def update_pos_bar(sec, freq=1, update=False):

            # Bound time to [ 1.0 / fps, duration - .001]
            if sec < 1.0 / fps:
                sec = 1.0 / fps
            elif sec + 1.0 / fps > duration - .001:
                player.signal.keepPlay = False
                player.clipCtrl.ctrlBtn.set_play_icon()
                sec = duration - .001 - 1.0 / fps

            # Update position slider
            if update or sec % (1 / freq) < 1:
                player.clipCtrl.posBar.update_duration_info(sec)
                player.clipCtrl.posBar.posSlider.setValue(sec)

            return sec

        # Initialize sound unit
        pg.mixer.quit()
        pg.mixer.init(audiofps, -8 * nbytes, clip.audio.nchannels, 1024)
        make_sound(1.0 / fps)

        t = 1.0 / fps
        _t, t0 = mark_time(t)
        while t < duration - .001:

            # Calculate Time to Wait
            time.sleep(max(.0, (t - _t) - (time.time() - t0)))

            # Stop or Move
            if player.signal.stopPlay:
                player.signal.stopPlay = False

                if player.signal.rel_pos:
                    t += player.signal.position
                else:
                    t = player.signal.position

                # Draw image of moved time
                make_image(t)

                # Update time info
                _t, t0 = mark_time(t)
                t = update_pos_bar(t, update=True)

            # Quit playing
            if player.signal.quitPlay:
                self.channel.stop()
                del self.channel
                return

            # Keep playing
            if player.signal.keepPlay:

                try:
                    make_image(t)
                    make_sound(t)
                except:
                    # Pause
                    player.play_clip()
                    pass

                t += 1.0 / fps
                t = update_pos_bar(t)

    def make_layout(self):
        from PyQt5.QtWidgets import QGridLayout, QVBoxLayout

        video_layout = QVBoxLayout()
        video_layout.addWidget(self.clipWidget, 2)
        video_layout.addWidget(self.frameBar.bar)
        video_layout.addLayout(self.clipCtrl.layout)

        self.layout = QGridLayout()
        self.layout.addLayout(self.list.layout, 1, 1)
        self.layout.addLayout(video_layout, 1, 2)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 3)
