import hashlib
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QComboBox, QFrame, QGridLayout, QLabel,
                             QScrollBar, QGraphicsOpacityEffect)


class TimeWidget(QWidget):

    def __init__(self):

        def make_time_label(label):

            """
            Write time tag on QLabel object

            :param label: scrollable object to write time tag
            :return:
            """

            # interval == 10 :: 10 pixels mean 1 second

            width = label.width()
            height = label.height()

            # Time tag has 10 secs increment
            n = int(width / 100)  # 100 = 10 * interval

            w = width / n
            h = height / 2

            for i in range(n):
                scale_line = QLabel(label)
                scale_line.setGeometry(i * w, h, w, h)

                duration = QLabel(label)
                duration.setAttribute(Qt.WA_TranslucentBackground)
                duration.setGeometry(i * w - w / 2, 0, w, h)

                t = i * 10  # 10 = interval
                hh = t // 3600
                mm = (t % 3600) // 60
                ss = (t % 3600) % 60

                duration.setText("%02d:%02d:%02d:%02d" % (hh, mm, ss, 0))
                duration.setAlignment(Qt.AlignCenter)

                for j in range(10):

                    inner = QLabel(scale_line)
                    if j == 0:
                        inner.resize(1, h)
                    elif j == 5:
                        inner.resize(1, h / 2)
                    else:
                        inner.resize(1, h / 3)

                    inner.setFrameShadow(QFrame.Plain)
                    inner.setFrameShape(QFrame.VLine)
                    inner.move((j / 10) * w, 0)

        super().__init__()

        # 'widget' is variable for first line, left side of sequence bar
        self.widget = QLabel("00:30:00:00")
        self.widget.setAlignment(Qt.AlignCenter)

        # 'line' is variable for first line, right side of sequence bar
        self.line = QWidget()
        self.line.setAttribute(Qt.WA_TranslucentBackground)

        # 'line-inner' is variable for scrollable object inside of 'line'
        self.line_inner = QWidget(self.line)
        self.line_inner.setAttribute(Qt.WA_TranslucentBackground)
        self.line_inner.setGeometry(0, 0, 18000, 40)

        # Write time tag on 'line-inner'
        make_time_label(self.line_inner)

        self.make_layout()

    def make_layout(self):

        self.layout = QGridLayout()
        self.layout.addWidget(self.widget, 1, 1)
        self.layout.addWidget(self.line, 1, 2)

        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 6)


class ComboBox(QComboBox):

    def __init__(self, line):

        super().__init__()

        # These two are needed for 'seq-changed' method
        self.label = line.label
        self.keys = line.keys

        # The number of key
        n_seq = len(line.keys) / 2

        for i_seq in range(int(n_seq)):
            self.addItem("Sequence " + str(i_seq + 1))

        self.currentIndexChanged.connect(self.seq_changed)
        self.seq_changed()

    def seq_changed(self):
        """
        Draw edge frame of Label of sequence, left side of sequence bar

        :return:
        """

        def make_edge_frame(index):
            selected = ["Video " + str(index), "Audio " + str(index)]

            # If key is selected, draw edge frame. If not, erase edge frame
            for key in self.keys:
                if key in selected:
                    self.label[key].setFrameStyle(QFrame.Box | QFrame.Sunken)
                else:
                    self.label[key].setFrameStyle(QFrame.NoFrame)

        # Since combobox index is start from 0, add 1
        idx = self.currentIndex() + 1

        # Draw edge frame
        make_edge_frame(idx)


class ScrollBar(QScrollBar):

    def __init__(self):

        super().__init__()

        self.setOrientation(Qt.Horizontal)
        self.setRange(0, 999)
        self.setPageStep(100)
        self.setSingleStep(1)


class LineWidget(QWidget):

    def __init__(self, width, height):

        super().__init__()

        # Variable used to make line
        self.line_width = width
        self.line_height = height

        # Make keys
        self.keys = []

        for idx in range(2):
            self.keys.append("Video " + str(idx + 1))
            self.keys.append("Audio " + str(idx + 1))

        # Make line objects (More info in 'make-lines' method)
        self.label = {}
        self.sequence = {}
        self.sequence_inner = {}
        self.sequence_cursor = {}
        self.sequence_cursor_time = 0

        for key in self.keys:
            self.make_lines(key)

        # Combobox and scrollbar under the sequence bar
        self.combobox = ComboBox(self)
        self.scroll_bar = ScrollBar()

        self.make_layout()

    def make_lines(self, key):

        """
        label: Left side of the sequence bar
        sequence: Right side of the sequence bar
        seq_inner: Scrollable object inside of sequence
        seq_cursor: Red line indicating time info

        :param key: Variable in keys
        :return:
        """

        def make_lbl(name):
            lbl = QLabel(name)
            lbl.setFrameStyle(QFrame.NoFrame)
            lbl.setAlignment(Qt.AlignCenter)
            lbl.setLineWidth(1)
            lbl.setMidLineWidth(1)

            return lbl

        def make_seq():
            seq = QWidget()
            seq.setStyleSheet("background-color: rgba(255, 255, 255, 128)")

            return seq

        def make_inner(target):

            seq_inner = QWidget(target)
            seq_inner.setGeometry(0, 0, width, height)

            return seq_inner

        def make_cursor(target):
            cursor = QFrame(target)
            cursor.setStyleSheet("background-color: red")
            cursor.setFrameShadow(QFrame.Plain)
            cursor.setFrameShape(QFrame.VLine)
            cursor.setLineWidth(-1)
            cursor.setGeometry(0, 0, 1, 100)

            return cursor

        width = self.line_width
        height = self.line_height

        self.label[key] = make_lbl(key)
        self.sequence[key] = make_seq()
        self.sequence_inner[key] = make_inner(self.sequence[key])
        self.sequence_cursor[key] = make_cursor(self.sequence_inner[key])

    def move_seq_cursor(self, pos):

        """
        Move all of sequence_cursor at the same time

        :param pos: Position, cursor will be moved to this point
        :return:
        """

        for key in self.keys:
            self.sequence_cursor[key].move(pos, 0)

    def make_layout(self):

        # Number of sequences. One sequence has 2 keys for video and audio
        n_sequence = int(len(self.keys) / 2)

        def add_lbl_and_seq(target):

            for index in range(1, 1 + n_sequence):
                keys = ["Video " + str(index), "Audio " + str(index)]
                pos = 2 * index - 1

                for key in keys:
                    target.addWidget(self.label[key], pos, 1)
                    target.addWidget(self.sequence[key], pos, 2)
                    pos = pos + 1

        self.layout = QGridLayout()
        add_lbl_and_seq(self.layout)

        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 6)


class SequenceWidget(QWidget):

    def __init__(self):

        super().__init__()

        # First line in sequence bar
        self.time = TimeWidget()
        self.part_width = self.time.line_inner.width()

        # Second to last line of sequence bar
        self.line = LineWidget(self.part_width, self.time.line.height())
        self.line.scroll_bar.valueChanged.connect(self.scroll_moved)

        # List for save labels of sequence bar
        self.clip_label_list = {}
        for key in self.line.keys:
            self.clip_label_list[key] = []

        self.make_layout()

    def make_label(self, clip_name, duration, start_at=None,
                   draw_video=True, draw_audio=True):

        """
        Draw label in sequence bar

        :param clip_name: clip's name
        :param duration: clip's duration, width of label
        :param start_at: Start point of label in time domain
        :param draw_video: bool, Draw in video sequence or not
        :param draw_audio: bool, Draw in audio sequence or not
        :return:
        """

        def get_color(string):

            """
            Decide color using hash algorithm

            :param string: Input of hashing

            :return: three integer in [0, 255]
            """

            color = hashlib.md5(string.encode('utf-8')).hexdigest()
            red = int(color[0:2], 16)
            green = int(color[2:4], 16)
            blue = int(color[4:6], 16)

            return red, green, blue

        def draw_label(key):

            """
            Draw label with color and clip_name

            :param key: key of selected sequence
            :return:
            """

            # Parent of the sequence label
            target = self.line.sequence_inner[key]

            # Draw label
            label = QLabel(target)
            QLabel(clip_name, label)
            opacity = QGraphicsOpacityEffect(label)
            opacity.setOpacity(0.3)
            label.setGraphicsEffect(opacity)
            label.setStyleSheet('background-color: rgb(%d, %d, %d)' % (r, g, b))
            label.setGeometry(x_pos, 0, duration * 10, 50)  # 10 = interval
            label.show()

            # Add to the list
            label.name = clip_name
            self.clip_label_list[key].append(label)

        def overlap_check(key):

            """
            Check if new label is overlapped with already existing one

            :param key: key of seleceted sequence

            :return: bool. True for overlapped. False for fine.
            """

            lbl_list = self.clip_label_list[key]

            is_overlap = False
            for i in range(len(lbl_list)):

                x = lbl_list[i].x()
                width = lbl_list[i].width()

                if x <= x_pos < x + width or x_pos <= x < x_pos + duration:
                    is_overlap = True

            return is_overlap

        # Combobox index starts from 0, when sequence index starts from 1
        idx = self.line.combobox.currentIndex() + 1

        v_key = "Video " + str(idx)
        a_key = "Audio " + str(idx)

        # If start point is None, get from cursor on sequence bar
        if start_at is None:
            start_at = self.line.sequence_cursor_time

        # Calculate pixel-wise coordinate
        x_pos = start_at * 10  # 10 = interval

        r, g, b = get_color(clip_name)

        is_video_overlap = overlap_check(v_key)
        is_audio_overlap = overlap_check(a_key)

        if not is_video_overlap and draw_video:
            draw_label(v_key)

        if not is_audio_overlap and draw_audio:
            draw_label(a_key)

    def make_sequence(self):

        """
        Add new Sequence to the Sequence Bar
        Initial number of Sequence is 2

        :return:
        """

        line = self.line

        # Index of new sequence
        idx = int(len(line.keys) / 2) + 1

        pos = 1
        for key in ["Video " + str(idx), "Audio " + str(idx)]:
            line.keys.append(key)           # Add new to key
            line.make_lines(key)            # Add new to object
            self.clip_label_list[key] = []  # Add new to list

            line.layout.addWidget(line.label[key], 2 * idx - pos, 1)
            line.layout.addWidget(line.sequence[key], 2 * idx - pos, 2)
            pos = pos - 1

        # Add new to combobox
        line.combobox.addItem("Sequence " + str(idx))

    def scroll_moved(self):

        """
        Move several widgets according to scrollbar

        :return:
        """

        pos = self.line.scroll_bar.sliderPosition()
        full_width = self.time.line.width()

        # Calculate movement
        interval = pos * (full_width - self.part_width) / 999

        # Move time widget and line widgets
        self.time.line_inner.move(interval, 0)
        for key in self.line.keys:
            self.line.sequence_inner[key].move(interval, 0)

    def make_layout(self):

        self.layout = QGridLayout()
        self.layout.addLayout(self.time.layout, 1, 1, 1, 2)
        self.layout.addLayout(self.line.layout, 2, 1, 1, 2)

        self.layout.addWidget(self.line.combobox, 999, 1)
        self.layout.addWidget(self.line.scroll_bar, 999, 2)

        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 6)
